library(survival)

# danach ignorieren und machen lassen
clincor=function(var, score, var.name, score.name, p.method="wilcox") {
  p=1
  if(p.method=="wilcox") p=wilcox.test(score~var)$p.value
  p=signif(p, 3)

  boxplot(score~var,
          range=0,
          ylab=score.name,
          xlab=var.name,
          lty=1,
          #main="Wilcoxon rank sum test",
          #ylim=c(0,600),
          frame=F
  )
  points(jitter(as.integer(factor(var))), jitter(score), cex=0.5)
  ttt=table(var, !is.na(score))[,"TRUE"]
  axis(3, at=1:length(ttt), labels=paste("n=", ttt, sep=""))
  mtext(paste("Wilcoxon rank sum test: p-value=", p, sep=""), 3, 3, adj=1)


}


# hier die folgenden Zeilen ans Ende kopieren wenn Anzahl Patienten gewünscht
# in der folgenden Zeile R$"P.Nummer" immer passend zur Spalte mit den IDs austauchen
#individuen = unique(knime.in$"P-Nummer")
#anzahlIndividuen = length(individuen)
#mtext(paste("n patients =", anzahlIndividuen, sep=""), 3, 0, adj=1)
# diese folgende Zeile in andere PlotProgrammcodes kopieren
# muss in der function am ende sein
#mtext(paste("n samples =", length(knime.in$"P-Nummer"), sep=""), 3, 1, adj=1)


