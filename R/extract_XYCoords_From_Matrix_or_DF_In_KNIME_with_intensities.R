


#' extract XYCoords from Matrix or DF e.g. in KNIME
#'
#' @param knime.in a df or matrix, e.g. an image matrix
#' @param cutoff intesity which should be used to cut the matrix in ROI and background
#'
#' @return the xy positions for the pixels at or above cutoff with their intensities in the third column
#' @export
#'
#' @examples
#' image_matrix <- matrix(rep(255, 25), ncol = 5)
#'  image_matrix[3,3] <- 666
#'  # 255 255 255 255 255
#'  # 255 255 255 255 255
#'  # 255 255 666 255 255
#'  # 255 255 255 255 255
#'  # 255 255 255 255 255
#'  extract_XYCoords_From_Matrix_or_DF_In_KNIME_with_intensities(image_matrix, cutoff = 256)
#'  # returns
#'  #      x_coords y_coords intensity
#'  # [1,]        3        3        666
extract_XYCoords_From_Matrix_or_DF_In_KNIME_with_intensities <- function (knime.in, cutoff = 1){
  
  #matrix style
  
  knime.in <- as.matrix(knime.in)
  
  
  #knime.in <- as.data.frame(mmm)
  
  # where are the ones with higher cutoff using which this way we get x and y in one shot
  x_y_z_of_those_were_bigger <-
    which(knime.in >= cutoff, arr.ind = TRUE)
  
  
  how_many_where_found <- length(x_y_z_of_those_were_bigger[, 1])
  
  # collector of intensities just x times the -1
  my_intensities <- rep(-1, how_many_where_found)
  
  
  for(index_of_intensity in 1:how_many_where_found){
   my_intensities[index_of_intensity] <- knime.in[x_y_z_of_those_were_bigger[index_of_intensity , 1] , x_y_z_of_those_were_bigger[index_of_intensity, 2]]
  }
    
  
  x_y_z_of_those_were_bigger_with_intensities <- data.frame(x_coords = x_y_z_of_those_were_bigger[ , 1], y_coords = x_y_z_of_those_were_bigger[ , 2], intensity = my_intensities)
  
  colnames(x_y_z_of_those_were_bigger_with_intensities) <- c("x_coords", "y_coords", "intensity")
  
  
  
  knime.out <- x_y_z_of_those_were_bigger_with_intensities
  
  return(knime.out)
  
}
