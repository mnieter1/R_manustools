#' add a global index column to a table
#'
#' just a convenience function for adding a global index to a table
#'
#' @param input_table just a table / data.frame
#'
#' @return a data frame with the global index attached
#' @export
#'
#' @examples
#' # create a table
#' my_table <- matrix(rep(0, 100), ncol = 2)
#' # add the index
#' my_table_w_global_index <- add_global_index_to_table(my_table)
#' # take a look
#' head(my_table_w_global_index)
add_global_index_to_table <-function(input_table){
  length_of_required_global_sequence  <- dim(input_table)[1]
  global_index <- 1:length_of_required_global_sequence
  input_table_w_global_indices <- data.frame(input_table, global_index)
  return(input_table_w_global_indices)
}
